<?header('Access-Control-Allow-Origin: *');?>
<!DOCTYPE html><html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<title>Управление детектором ZnS+Li</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
		<script src="https://kaf24.mephi.ru/smart-lab/zns/api.js"></script>		
		<style>
			@import url('https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,700;1,400;1,700&display=swap');			
			body{margin:0;padding:0;font-family:'Roboto',sans-serif;height: 100%;}
			.layout{width:340px;padding:10px 20px;margin:0 auto;  display: flex;flex-direction: column;height: 100%;}
			.header{position:relative;width:100%;height:60px;}
			.header__logo{position:absolute;width:80px;height:60px;left:0;top:0;}
			.header__title{position:absolute;height:60px;left:100px;right:0;top:0;}
			.title{position:relative;width:100%;height:100%}
			.title__h1{font-size:18px;padding:0;text-align:center;width:100%;line-height:30px;}			
			.workarea{margin:20px 0;}
			.description{font-size:14px; margin-bottom:20px; text-align:center}
			.footer{flex: 0 0 auto;}
			.copyright{font-size:12px;line-height:12px;}
			.timer{margin:20px 0; position:relative;height:48px;}
			.timer__header{position:absolute; width: 120px; height:100%;left:0;top:0;font-size:24px;line-height:48px;}
			.timer__value{position:absolute; padding:10px;line-height:24px; height:auto;left:140px;top:0;right:0;font-size:24px; font-weight:bold;border: 1px solid #CCC;}
			.counter{margin:20px 0; position:relative;height:48px;}
			.counter__header{position:absolute; width: 120px; height:100%;left:0;top:0;font-size:24px;line-height:48px;}
			.counter__value{position:absolute; right:0; padding:10px;line-height:24px; height:auto;left:140px;top:0; font-size:24px; font-weight:bold;border: 1px solid #CCC;text-align:right}
			.button{margin:20px 0; position:relative; height:48px;text-align:center}
			.button__start{display:inline-block;background-color:#3498db;border: none;color: white;padding:12px 24px;text-decoration: none;font-size: 24px; margin: 0px auto; cursor: pointer;border-radius: 4px;line-height: 24px;}
			.button__text{display:inline-block;background-color:#34495e;border: none;color: white;padding:12px 24px;text-decoration: none;font-size: 24px; margin: 0px auto; cursor: pointer;border-radius: 4px;line-height: 24px;}
		</style>
		
	</head>
	<body>
		<div class="layout">
			<div class="header">
				<div class="header__logo">
					<div class="logo">
						<img class="logo-img" src="http://kaf24.mephi.ru/smart-lab/images/logo.jpg"/>
					</div>		
				</div>
				<div class="header__title">
					<div class="title">
						<h1 class="title__h1">Детектор ZnS+Li</h1>	
					</div>
				</div>
			</div>
			<div class="workarea">
				<div class="description">Длительность одного измерения 100 с. <br> Если измерение запущено на экране будет отображено прошедшее время <br>и накопленный счет.</div>
				<div class="timer">
					<div class="timer__header">Время:</div>
					<div class="timer__value" id="timer">--- c</div>
				</div>
				
				<div class="counter">
					<div class="counter__header">Счетчик 1:</div>
					<div class="counter__value" id="counter1">0</div>
				</div>
				<div class="counter">
					<div class="counter__header">Счетчик 2:</div>
					<div class="counter__value" id="counter2">0</div>
				</div>
				
				<div class="button">
					<div class="button__start" id="bntStart">Старт</div>
				</div>						
			</div>
			<div class="footer">
				<div class="copyright">МИФИ, "Прикладая ядерная физика"</div>
			</div>
		</div>
	</body>
</html>
<script>
$(document).ready(function(){
	$("body").on("click", "#bntStart", function(){
		$.ajax({
			url: '/start',
			method: 'get',
			dataType: 'html',
			data: {},
			success: function(data){
				$(".button").html('<div class="button__text" >Идет процесс набора данных</div>');
				$("#timer").text('0 c');
			}
		});		
		
		timerId = setTimeout(function run(){
			$.ajax({
				url: '/reload',
				method: 'get',
				dataType: 'json',
				data: {},
				success: function(data){
					console.log(data);
					if (data['flgMeasure']){
						$(".button").html('<div class="button__text" >Идет процесс набора данных</div>');						
					}else {
						$(".button").html('<div class="button__start" id="bntStart">Старт</div>');
					}
					$("#counter1").text(data['Counter1']);
					$("#counter2").text(data['Counter2']);
					$("#timer").text(data['TimePass']+' c');
					
				}			
			});
			setTimeout(run, 1000);
		}, 100)
	});
})
</script>