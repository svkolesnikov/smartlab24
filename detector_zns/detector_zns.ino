// Author: Svyatoslav Kolesnikov
// Copyright: © 2022, Svyatoslav Kolesnikov
// License: Apache software license
// web: https://gitlab.com/svkolesnikov/smartlab24

#include <WiFi.h>
#include <WebServer.h>
#include <ArduinoJson.h>// Требуется установить через менеджер библиотек

// Установите здесь свои SSID и пароль от WiFi
const char* ssid = "Имя сети WiFI";  
const char* password = "Пароль от WiFi";  

// Определяем какие два контакта использыем для получения сигнала от детектора
#define PIN1 12
#define PIN2 14
#define PULSE_DURATION 2         // длительность TTL имппульса в мск (нужно чтобы избежать срабатывание счетчиков на "дребезг")

int timer = 100;                 // Таймер - длительность измерения, 100 секунд
int timeStart = 0;               // Время в мс, когда началось измерение
bool flgMeasure = false;         // флаг, что происходит измерение, true - измерение идет, false - измерения нет

// Счетчики в которых накапливается счет после начала измерения
int counter1 = 0;
int counter2 = 0;

// Создаем web - сервер на 80 порту
WebServer server(80);

void setup() {

  // Добавляем режим отладки 
  Serial.begin(115200);
  delay(100);

  // Устанавливаем тип работы пинов
  pinMode(PIN1, INPUT);
  pinMode(PIN2, INPUT);

  // Вешаем на пины прерывание
  attachInterrupt(PIN1, addCount1, RISING);
  attachInterrupt(PIN2, addCount2, RISING);
    
  Serial.println("Connecting to ");
  Serial.println(ssid);

  //connect to your local wi-fi network
  WiFi.begin(ssid, password);
  //check wi-fi is connected to wi-fi network
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  
  Serial.println("");
  Serial.println("WiFi connected..!");
  Serial.print("Got IP: ");  Serial.println(WiFi.localIP());
  
  // Определяем на какие команды должен отвечать сервер
  server.on("/", handle_OnConnect);         // загрузка основной страницы
  server.on("/start", handle_start);        // запустить измерение
  server.on("/stop", handle_stop);          // остановить измерение
  server.on("/reload", handle_reload);      // обновить данные 
  
  server.onNotFound(handle_NotFound);
  server.begin();
  Serial.println("HTTP server started");
}

void loop() {
  server.handleClient();

  if (millis() > timer*1000 + timeStart && flgMeasure){
    Serial.println("Measurement is stop");
    flgMeasure = false;
  }
}

void addCount1(){
  if (flgMeasure) {
    counter1++;
    delayMicroseconds(PULSE_DURATION);
  }  
}

void addCount2(){
  if (flgMeasure) {
    counter2++;
    delayMicroseconds(PULSE_DURATION);
  }  
}

void handle_OnConnect() {
  Serial.println("Connect");
  server.send(200, "text/html", SendHTML()); 
}

void handle_start() {
  timeStart = millis();
  Serial.println("Measurement is start. Timer: 100c");
  counter1 = 0;
  counter2 = 0;
  flgMeasure = true;
      
  server.send(200, "text/html", SendHTML()); 
}

void handle_stop() {  
  Serial.println("Measurement is stop. Time = " + (timer*1000 + timeStart - millis()) );  
  flgMeasure = false;        
  server.send(200, "text/html", SendHTML()); 
}

void handle_reload() {
  String buffer;
  Serial.println("Reload data");  

  StaticJsonDocument<100> json;
  json["flgMeasure"] = flgMeasure;
  json["Counter1"] = counter1;
  json["Counter2"] = counter2;
  json["Timer"] = timer;
  if (timer*1000 + timeStart > millis() && flgMeasure) {
     json["TimePass"] = (millis() - timeStart)/1000  ; // возможно отрицательное число  
  }
  else {
    json["TimePass"] = 100;
  }
  
  serializeJsonPretty(json, buffer); 
  server.send(200, "text/json", buffer); 
}

void handle_NotFound(){
  server.send(404, "text/plain", "Not found");
}

// SendHTML - функция формирует html страницу по умолчанию.
String SendHTML(){
  String ptr = "<!DOCTYPE html> <html>\n";
  ptr +="<head><meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
  ptr +="<title>Управление светодиодом</title>\n";  
  ptr +="<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js\"></script>";
  ptr +="<script>$(document).ready(function(){$(\"body\").load( \"https://kaf24.mephi.ru/smart-lab/zns/\")})</script>";
  ptr +="</head>\n";
  ptr +="<body>\n";
  ptr +="<h1>Детектор ZnS+Li</h1>\n";
  
  ptr +="</body>\n";
  ptr +="</html>\n";
  return ptr;
}
